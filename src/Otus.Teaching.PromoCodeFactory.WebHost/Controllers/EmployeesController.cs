﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Контроллер для работы с сотрудниками
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [OpenApiTag("Контроллер для работы с сотрудниками")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles?.Select(x => new RoleItemResponse
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromoCodesCount = employee.AppliedPromoCodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создание сотрудника
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<EmployeeShortResponse>> CreateEmployeeAsync(UpsertEmployeeRequest request)
        {
            var employee = await _employeeRepository.CreateAsync(request.Adapt<Employee>());
            return employee.Adapt<EmployeeShortResponse>();
        }

        /// <summary>
        /// Обновление данных сотрудника
        /// </summary>
        [HttpPut("{id:Guid}")]
        public async Task<ActionResult> UpdateEmployeeAsync(Guid id, UpsertEmployeeRequest request)
        {
            try
            {
                var employee = request.Adapt<Employee>();
                employee.Id = id;
                await _employeeRepository.UpdateAsync(employee);
                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return BadRequest($"User with Id = {id} not exists");
            }
            catch
            {
                return new ObjectResult("Internal server error")
                {
                    StatusCode = 500
                };
            }
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        [HttpDelete("{id:Guid}")]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            try
            {
                await _employeeRepository.DeleteAsync(id);
                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return BadRequest($"User with Id = {id} not exists");
            }
            catch
            {
                return new ObjectResult("Internal server error")
                {
                    StatusCode = 500
                };
            }
        }
    }
}