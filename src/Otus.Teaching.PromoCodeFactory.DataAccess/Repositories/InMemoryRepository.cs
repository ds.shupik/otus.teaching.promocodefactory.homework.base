﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data = Data.Append(entity);
            return Task.FromResult(entity);
        }

        public Task UpdateAsync(T entity)
        {
            var existingEntity = Data.FirstOrDefault(s => s.Id == entity.Id);
            if (existingEntity == null) throw new KeyNotFoundException();
            entity.Adapt(existingEntity);
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            var entity = Data.FirstOrDefault(s => s.Id == id);
            if (entity == null) throw new KeyNotFoundException();
            Data = Data.Where(s => s.Id != id);
            return Task.CompletedTask;
        }
    }
}